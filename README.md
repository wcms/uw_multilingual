# Uw Multilingual module.
This module provide basic necessary configuration to update the current site language to another language
Also, the module provide an architecture of plugin manager for new configuration needed be added in an easy way.

## How the module works and how every setting is set up.

There is a service called `uw_multilingual.setup` that is responsible to manage the plugins defined to do set up of every needed setting. <br />
The method `setupSiteConfiguration` create a batch that group every setting operation, which operation is plugin definition.<br />
The method use the `UwMLSetupPluginManager` to load all plugins defined and for each plugin an operation batch will be created.<br />
Every plugin is responsible to do a unique setting for the all settings needed. The plugins also is responsible to get the data(data returned from `getData`) that will be used to set up the plugin setting.<br />
Also, before the plugin be added on the queue operation execution a validation method is called. Each plugin implement this method that check if the plugin is available to be executed.<br />
Follow some reasons that the plugin cannot be executed or will be just skipped.<br />
* Plugin already executed, for some reason the setup needs to run again and the plugin is already did the job.<br />
* Plugin dependency failed, this dependency can be defined on this method, like a check if directory to download translation files is ok.<br />
* There is no data to be translated based on the plugin entity criteria.<br />

## How to use it.
Access the url: `/admin/appearance/multilingual-setup`, select the new language and set the other settings and submit.<br />
Batches will be initialized running the necessary configuration defined by the plugins.

#### **_If any other configuration must be done, create a new plugin to do that and the plugin manager handle everything need._**

## Create a new plugin of UwMLSetup.
For a new entity plugin take a loot at `UwMLSetupPluginTranslateEntityBase` and any other plugin that extends it.
If the entity do not need any special criteria or handler, just add the entity type id on the `$entities` array on deriver class `UwMLSetupPluginDeriver`. <br>
Otherwise you can extend the `UwMLSetupPluginTranslateEntityBase` and do the special handler to new plugin entity. Take a loot at `UwMLSetupTranslateMenuEntities` or `UwMLSetupTranslateRedirectEntities` where we have different behavior.

If it's another kind of configuration just implement the logic to retrive the configuration data on `getData` plugin method. <br />
After that implement the logic to do the configuration on `processData` method.
Also, if you need to validate the plugin execution just implement this validation on `validateData` method.

### Below the plugin annotation.
```sh
/**
 * Describe that your plugin does here.
 *
 * @UwMLSetup(
 *   id = "uw_ml_setup_your_plugin_id",
 *   label = "Example plugin label.",
 *   description = "Example plugin description",
 *   weight = 10,
 *   batchLabel = "Example label that will be displayed on the batch operation."
 *   multipleOperation = true
 * )
 */
```

### Explaining annotation values:
* `id`, label and description define common information as usual.
* `weight`: defined the weight that plugin will be executed. Be attention to not put 1 or 2, because the plugin to add the new language must be executed first. Also, also this value can be used to control if your plugin needs to execute after of before any other plugin.
* `batchLabel`: To identify your plugin in execution time you can provide value and them when operation(s) is executing you will be able to have this status.
* `multipleOperation`: In cases that the plugin retrieve a lot of data, like process any kind of entity. Defining this value as `True` the batch will create one operation for each data returned on the `getData` method.

## Existing plugins explanations.

* `uw_ml_setup_add_language`: Basically create new ConfigurableLanguage using the language code and set this new language as default on the site.
  * The admin area that this plugin take over is: `/admin/config/regional/language`
* `uw_ml_setup_configure_language_detection`: Enable the language detection option _Account administration pages_ and put the options in the following weight:
  * Account administration pages
  * URL
  * Session
  * User
  * Browser
  * Selected language
    * The relative admin are is: `admin/config/regional/language/detection`
* `uw_ml_setup_preferred_lang_users`: Load all users that has these roles(`uw_ml_setup_preferred_lang_users, uw_role_site_owner, uw_role_content_editor, uw_role_content_author`) and update their admin language preference to English.
* `uw_ml_setup_update_entity`: Update all entities from entity type defined on deriver `UwMLSetupPluginDeriver`
