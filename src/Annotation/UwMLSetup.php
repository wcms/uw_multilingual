<?php

namespace Drupal\uw_multilingual\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Annotation for UwMLSetup plugins.
 *
 * @Annotation
 */
class UwMLSetup extends Plugin {

  /**
   * Plugin id.
   *
   * @var string
   */
  public $id;

  /**
   * Plugin label.
   *
   * @var string
   */
  public $label;

  /**
   * Plugin description.
   *
   * @var string
   */
  public $description;

  /**
   * Execution plugin weight.
   *
   * @var int
   */
  public $weight;

  /**
   * Batch label.
   *
   * @var string
   */
  public $batchLabel;

  /**
   * Plugin flag to define how the data will be executed.
   *
   * @var bool
   */
  public $multipleOperation;

}
