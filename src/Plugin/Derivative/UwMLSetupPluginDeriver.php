<?php

namespace Drupal\uw_multilingual\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Deriver for UwMLSetup base entity to configure all entities to be updated.
 */
class UwMLSetupPluginDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * Entities to be updated.
   *
   * @var string[]
   */
  protected $entities = [
    'user',
    'bibcite_contributor',
    'bibcite_keyword',
    'bibcite_reference',
    'block_content',
    'block',
    'file',
    'media',
    'node',
    'taxonomy_term',
    'webform_submission',
  ];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    // Do nothing.
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach ($this->entities as $entity) {
      $this->derivatives[$entity] = $base_plugin_definition;
      $this->derivatives[$entity]['id'] = 'uw_ml_setup_translate_' . $entity . '_entities';
      $this->derivatives[$entity]['entity'] = $entity;
      $this->derivatives[$entity]['label'] = "UW Translate {$entity} Entities";
    }
    return $this->derivatives;
  }

}
