<?php

namespace Drupal\uw_multilingual\Plugin\UwMLSetup;

use Drupal\uw_multilingual\UwMLSetupPluginTranslateEntityBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin to translate all paragraph entities.
 *
 * @UwMLSetup(
 *   id = "uw_ml_setup_translate_paragraph_entities",
 *   label = "UW Translate Paragraph Entities",
 *   description = "Update langugage code of all paragraphs",
 *   weight = 6,
 *   batchLabel = "Update language code of paragraph @label",
 *   multipleOperation = true
 * )
 */
class UwMLSetupTranslateParagraphEntities extends UwMLSetupPluginTranslateEntityBase {

  /**
   * Module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ContainerInterface $container) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $container);
    $this->moduleHandler = $container->get('module_handler');
  }

  /**
   * {@inheritdoc}
   */
  public function validateData() {
    if ($this->moduleHandler->moduleExists('paragraphs')) {
      return self::EXECUTION_STATUS['EXECUTE'];
    }
    return self::EXECUTION_STATUS['SKIP'];
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityType(): string {
    return 'paragraph';
  }

}
