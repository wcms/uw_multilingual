<?php

namespace Drupal\uw_multilingual\Plugin\UwMLSetup;

use Drupal\uw_multilingual\UwMLSetupPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin to define operation to configure language detection.
 *
 * This plugin do the following configuration:
 * Enable Account administration pages language setting.
 * Configure path prefix of the domains.
 *
 * @UwMLSetup(
 *   id = "uw_ml_setup_configure_language_detection",
 *   label = "UW Configure Languge Detection",
 *   description = "Configure Language Detection Page",
 *   weight = 2,
 *   batchLabel = "Configuring language detection"
 * )
 */
class UwMLSetupLanguageDetection extends UwMLSetupPluginBase {

  /**
   * The block manager.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  public $blockManager;

  /**
   * The language negotiator.
   *
   * @var \Drupal\language\LanguageNegotiatorInterface
   */
  public $negotiator;

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  public $configFactory;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  public $languageManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ContainerInterface $container) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $container);
    $this->negotiator = $container->get('language_negotiator');
    $this->blockManager = $container->get('plugin.manager.block');
    $this->configFactory = $container->get('config.factory');
    $this->languageManager = $container->get('language_manager');
  }

  /**
   * {@inheritdoc}
   */
  public function getData(): array {
    return [
      'language_interface' => [
        'weights' => [
          'language-user-admin' => -10,
          'language-url' => -8,
          'language-selected' => 12,
        ],
        'inputs' => [
          'language-user-admin' => -10,
          'language-url' => -8,
          'language-session' => -6,
          'language-user' => -4,
          'language-browser' => -2,
          'language-selected' => 12,
        ],
      ],
      'language_content' => [
        "weights" => [
          'language-interface' => 9,
          'language-selected' => 12,
        ],
        'inputs' => [
          "language-content-entity" => -9,
          "language-url" => -8,
          "language-session" => -6,
          "language-user" => -4,
          "language-browser" => -2,
          "language-interface" => 9,
          "language-selected" => 12,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function processData($data, &$context = []) {
    $langcode = $this->configuration['language'];
    foreach ($data as $type => $language_config_weight) {
      $this->configFactory->getEditable('language.types')
        ->set('negotiation.' . $type . '.method_weights', $language_config_weight['inputs'])->save();
    }
    foreach ($data as $type => $language_config_weight) {
      $this->negotiator->saveConfiguration($type, $language_config_weight['weights']);
    }
    $this->blockManager->clearCachedDefinitions();

    // Configure url domains and prefixes to new language.
    // For the default language remove the uri as lang code.
    $languages = $this->languageManager->getLanguages();
    $url_prefixes = [];
    $url_domains = [];
    foreach (array_keys($languages) as $lang) {
      $url_prefixes[$lang] = $lang === $langcode ? '' : $lang;
      $url_domains[$lang] = '';
    }
    $this->configFactory->getEditable('language.negotiation')
      ->set('url.source', 'path_prefix')
      // Save new domain and prefix values.
      ->set('url.prefixes', $url_prefixes)
      ->set('url.domains', $url_domains)
      ->save();
    $context['message'] = $this->t('Configuring language detection.');
  }

}
