<?php

namespace Drupal\uw_multilingual\Plugin\UwMLSetup;

use Drupal\uw_multilingual\UwMLSetupPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin to set new default website language.
 *
 * @UwMLSetup(
 *   id = "uw_ml_setup_configure_new_language",
 *   label = "Set new side language",
 *   description = "Set new default website language",
 *   weight = 1,
 *   batchLabel = "Set up new site language"
 * )
 */
class UwMLSetupNewDefaultLanguage extends UwMLSetupPluginBase {

  use UwMLSetupValidateTrait;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  public $languageManager;

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  public $configFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ContainerInterface $container) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $container);
    $this->configFactory = $container->get('config.factory');
    $this->languageManager = $container->get('language_manager');
  }

  /**
   * {@inheritdoc}
   */
  public function validateData() {
    if (!$this->isValidLanguage($this->configuration['language'])) {
      throw new \Exception('Only the following langcodes are available: ' . implode(', ', $this->getValidLanguages()));
    }
    return self::EXECUTION_STATUS['EXECUTE'];
  }

  /**
   * {@inheritdoc}
   */
  public function getData(): array {
    return ['langcode' => $this->configuration['language']];
  }

  /**
   * {@inheritdoc}
   */
  public function processData($data, &$context = []) {
    $this->configFactory->getEditable('system.site')
      ->set('default_langcode', $data['langcode'])->save();
    $this->languageManager->reset();
    $context['message'] = $this->batchLabel();
  }

}
