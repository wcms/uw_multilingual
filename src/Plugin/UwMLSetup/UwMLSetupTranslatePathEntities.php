<?php

namespace Drupal\uw_multilingual\Plugin\UwMLSetup;

use Drupal\uw_multilingual\UwMLSetupPluginTranslateEntityBase;

/**
 * Plugin to update all path language.
 *
 * @UwMLSetup(
 *   id = "uw_ml_setup_translate_path_entities",
 *   label = "UW Translate Path Entities",
 *   description = "Update langugage code of all path",
 *   weight = 3,
 *   batchLabel = "Update language code of @label",
 *   multipleOperation = true
 * )
 */
class UwMLSetupTranslatePathEntities extends UwMLSetupPluginTranslateEntityBase {

  /**
   * {@inheritdoc}
   */
  public function getEntityType(): string {
    return 'path_alias';
  }

  /**
   * {@inheritdoc}
   */
  public function getCriteria() {
    return [
      'langcode' => [[$this->configuration['language'], 'und'], 'NOT IN'],
    ];
  }

}
