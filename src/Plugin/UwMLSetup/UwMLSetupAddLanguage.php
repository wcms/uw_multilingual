<?php

namespace Drupal\uw_multilingual\Plugin\UwMLSetup;

use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\uw_multilingual\UwMLSetupPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin to define operation to create new language on setup.
 *
 * @UwMLSetup(
 *   id = "uw_ml_setup_add_language",
 *   label = "Create new language",
 *   description = "Create new language and set as default",
 *   weight = 1,
 *   batchLabel = "Setup new language"
 * )
 */
class UwMLSetupAddLanguage extends UwMLSetupPluginBase {

  use UwMLSetupValidateTrait;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  public $languageManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ContainerInterface $container) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $container);
    $this->languageManager = $container->get('language_manager');
  }

  /**
   * {@inheritdoc}
   */
  public function getData(): array {
    return ['langcode' => $this->configuration['language']];
  }

  /**
   * {@inheritdoc}
   */
  public function validateData() {
    $languages = $this->languageManager->getLanguages();
    if (in_array($this->configuration['language'], array_keys($languages))) {
      return self::EXECUTION_STATUS['SKIP'];
    }
    return self::EXECUTION_STATUS['EXECUTE'];
  }

  /**
   * {@inheritdoc}
   */
  public function processData($data, &$context = []) {
    $new_language = ConfigurableLanguage::createFromLangcode($data['langcode']);
    $new_language->save();
  }

}
