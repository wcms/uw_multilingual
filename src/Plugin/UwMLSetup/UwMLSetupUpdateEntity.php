<?php

namespace Drupal\uw_multilingual\Plugin\UwMLSetup;

use Drupal\uw_multilingual\UwMLSetupPluginTranslateEntityBase;

/**
 * Plugin to update entity language.
 *
 * @UwMLSetup(
 *   id = "uw_ml_setup_update_entity",
 *   label = "Update language code.",
 *   description = "Update all entities langcpde",
 *   weight = 4,
 *   batchLabel = "Update language code of @label",
 *   multipleOperation = true,
 *   deriver = "Drupal\uw_multilingual\Plugin\Derivative\UwMLSetupPluginDeriver"
 * )
 */
class UwMLSetupUpdateEntity extends UwMLSetupPluginTranslateEntityBase {

  /**
   * {@inheritdoc}
   */
  public function getEntityType(): string {
    return $this->pluginDefinition['entity'];
  }

}
