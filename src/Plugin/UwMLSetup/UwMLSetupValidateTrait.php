<?php

namespace Drupal\uw_multilingual\Plugin\UwMLSetup;

/**
 * Trait to help test language setup.
 */
trait UwMLSetupValidateTrait {

  /**
   * Get valid lang codes.
   *
   * @return string[]
   *   Array of valid lang codes.
   */
  public function getValidLanguages() {
    return [
      'en',
      'fr',
      'zh-hans',
    ];
  }

  /**
   * Check if language is available to configure.
   *
   * @param string $language
   *   Lancode.
   *
   * @return bool
   *   True for language available, otherwise false.
   */
  public function isValidLanguage(string $language) {
    return in_array($language, $this->getValidLanguages());
  }

}
