<?php

namespace Drupal\uw_multilingual\Plugin\UwMLSetup;

use Drupal\uw_multilingual\UwMLSetupPluginTranslateEntityBase;

/**
 * Plugin to update all redirect language.
 *
 * @UwMLSetup(
 *   id = "uw_ml_setup_translate_redirect_entities",
 *   label = "UW Translate Redirect Entities",
 *   description = "Update langugage code of all redirect",
 *   weight = 3,
 *   batchLabel = "Update language code of @label",
 *   multipleOperation = true
 * )
 */
class UwMLSetupTranslateRedirectEntities extends UwMLSetupPluginTranslateEntityBase {

  /**
   * {@inheritdoc}
   */
  public function getEntityType(): string {
    return 'redirect';
  }

  /**
   * {@inheritdoc}
   */
  public function getLanguageColumnName() {
    return 'language';
  }

  /**
   * {@inheritdoc}
   */
  public function getCriteria() {
    return [
      $this->getLanguageColumnName() => [
        [
          $this->configuration['language'],
          'und',
        ],
        'NOT IN',
      ],
    ];
  }

}
