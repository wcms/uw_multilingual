<?php

namespace Drupal\uw_multilingual\Plugin\UwMLSetup;

use Drupal\uw_multilingual\UwMLSetupPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin to update all users preferred admin language to English.
 *
 * @UwMLSetup(
 *   id = "uw_ml_setup_preferred_lang_users",
 *   label = "Configure prefered language for admin users.",
 *   description = "Update all users admin prefered language to English",
 *   weight = 3,
 *   batchLabel = "Configuring prefered admin language users.",
 *   multipleOperation = true
 * )
 */
class UwMLSetupPreferredLanguageUsers extends UwMLSetupPluginBase {

  /**
   * Storage user service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storageUsers;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ContainerInterface $container) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $container);
    $this->storageUsers = $container->get('entity_type.manager')->getStorage('user');
  }

  /**
   * {@inheritdoc}
   */
  public function getData(): array {
    $roles = [
      'uw_role_site_manager',
      'uw_role_site_owner',
      'uw_role_content_editor',
      'uw_role_content_author',
    ];

    $query = $this->storageUsers
      ->getQuery();

    $query->condition('status', 1);
    $query->condition('roles', $roles, 'IN');
    $query->condition('preferred_admin_langcode', 'en', '!=');
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function processData($data, &$context = []) {
    $context['message'] = $this->batchLabel();
    $user = $this->storageUsers->load($data);
    $user->set('preferred_admin_langcode', 'en');
    $user->save();
  }

}
