<?php

namespace Drupal\uw_multilingual\Plugin\UwMLSetup;

use Drupal\uw_multilingual\UwMLSetupPluginTranslateEntityBase;

/**
 * Plugin to update all menu languages.
 *
 * @UwMLSetup(
 *   id = "uw_ml_setup_translate_menu_entities",
 *   label = "UW Translate Menu Entities",
 *   description = "Update langugage code of all menus",
 *   weight = 5,
 *   batchLabel = "Update language code of menu @label",
 *   multipleOperation = true
 * )
 */
class UwMLSetupTranslateMenuEntities extends UwMLSetupPluginTranslateEntityBase {

  /**
   * {@inheritdoc}
   */
  public function getEntityType(): string {
    return 'menu';
  }

  /**
   * {@inheritdoc}
   */
  public function getCriteria() {
    $excluded_menus = [
      'admin',
      'uw-menu-content-management',
      'devel',
      'footer',
      'uw-menu-global-footer',
      'uw-menu-global-header',
      'uw-menu-global-social-media',
      'uw-menu-audience-menu',
      'main',
      'uw-menu-site-management',
      'tools',
      'account',
    ];
    return parent::getCriteria() + ['id' => [$excluded_menus, 'NOT IN']];
  }

}
