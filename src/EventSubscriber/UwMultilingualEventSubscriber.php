<?php

namespace Drupal\uw_multilingual\EventSubscriber;

use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Listens for language configuration changes.
 */
class UwMultilingualEventSubscriber implements EventSubscriberInterface {

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new UwMultilingualEventSubscriber.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   */
  public function __construct(LoggerInterface $logger) {
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Listen to config save event.
    $events[ConfigEvents::SAVE][] = 'onLanguageConfigChange';
    return $events;
  }

  /**
   * Triggered when language configuration is saved.
   */
  public function onLanguageConfigChange(ConfigCrudEvent $event) {
    // Check if the changed config is the language settings.
    $config = $event->getConfig();
    if ($config->getName() == 'system.site') {

      $original_default_languagecode = $config->getOriginal('default_langcode') ?? '';
      $default_languagecode = $config->get('default_langcode') ?? '';

      if ($original_default_languagecode != $default_languagecode) {
        // Custom logic triggered when the default language changes.
        $this->logger->notice('Default language switched to: @language', ['@language' => $default_languagecode]);

        // Define "Home" in our supported languages.
        $hometitle = [
          'en' => 'Home',
          'fr' => 'Accueil',
          'zh-hans' => '首页',
        ];

        // If page front is a node (it's possible that it's not node 1)
        // and its title is "Home" (in the current language), then change it.
        $front_page_node = $config->get('page')['front'];
        if (preg_match('/\/node\/(\d+)/', $front_page_node, $node_parts)) {
          $node_storage = \Drupal::entityTypeManager()->getStorage('node');
          $node = $node_storage->load($node_parts[1]);
          if (array_key_exists($original_default_languagecode, $hometitle) && array_key_exists($default_languagecode, $hometitle) && $node->getTitle() == $hometitle[$original_default_languagecode]) {
            $node->setTitle($hometitle[$default_languagecode]);
            $node->setRevisionLogMessage('Automatically changed the page title to the current site language.');
            $node->save();
          }
        }
      }
    }
  }

}
