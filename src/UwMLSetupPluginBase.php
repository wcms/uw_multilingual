<?php

namespace Drupal\uw_multilingual;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base Multiple Lingual setup plugin.
 */
abstract class UwMLSetupPluginBase extends PluginBase implements ContainerFactoryPluginInterface, UwMLSetupInterface {

  const EXECUTION_STATUS = [
    'STOP_EXECUTION' => -1,
    'SKIP' => 0,
    'EXECUTE' => 1,
  ];

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ContainerInterface $container) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container
    );
  }

  /**
   * Return the plugin id.
   *
   * @return string
   *   The id configured on the plugin definition.
   */
  public function id() {
    return $this->pluginDefinition['id'];
  }

  /**
   * The description of the plugin.
   *
   * @return string
   *   Description defined on the plugin.
   */
  public function description() {
    return $this->pluginDefinition['description'];
  }

  /**
   * Plugin label.
   *
   * @return string
   *   The label defined on the plugin.
   */
  public function label() {
    return $this->pluginDefinition['label'];
  }

  /**
   * The weight of the plugin on execute queue.
   *
   * @return int
   *   The position of the weight.
   */
  public function weight() {
    return (int) $this->pluginDefinition['weight'];
  }

  /**
   * The label to be displayed on the batch execution.
   *
   * @return string
   *   The label of the content plugin.
   */
  public function batchLabel() {
    return $this->pluginDefinition['batchLabel'];
  }

  /**
   * Flag to define how the data will be processed.
   *
   * In a group the operations or a unique operation.
   *
   * @return bool
   *   True if the plugin will execute in multiple operations, otherwise one.
   */
  public function multipleOperation() {
    return $this->pluginDefinition['multipleOperation'] ?? FALSE;
  }

  /**
   * Override this method and implement the custom validation.
   *
   * @return int
   *   By default, execute the plugin.
   */
  public function validateData() {
    return self::EXECUTION_STATUS['EXECUTE'];
  }

}
