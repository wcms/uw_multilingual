<?php

namespace Drupal\uw_multilingual\Service;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\uw_multilingual\UwMLSetupPluginBase;
use Symfony\Component\Finder\Finder;

/**
 * Provide methods to configure the language of the website.
 */
class UwMultiLingualSetupService implements UwMultiLingualSetupInterface {

  use StringTranslationTrait;

  /**
   * Plugin manager service.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $uwMLSetupPLuginManger;

  /**
   * Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Initiate service dependencies.
   *
   * @param \Drupal\Component\Plugin\PluginManagerInterface $uwMLSetupPluginManger
   *   Plugin manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module handler service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory service.
   */
  public function __construct(PluginManagerInterface $uwMLSetupPluginManger, MessengerInterface $messenger, ModuleHandlerInterface $moduleHandler, ConfigFactoryInterface $configFactory) {
    $this->uwMLSetupPLuginManger = $uwMLSetupPluginManger;
    $this->messenger = $messenger;
    $this->moduleHandler = $moduleHandler;
    $this->configFactory = $configFactory;
  }

  /**
   * Setup all language settings for a new language.
   *
   * @param array $config
   *   Config to be used on the setup.
   */
  public function setupSiteConfiguration(array $config = []) {
    $batch_builder = (new BatchBuilder())
      ->setTitle('Updating content languages')
      ->setErrorMessage($this->t('Error to update content language'))
      ->setFinishCallback([self::class, 'updateContentLanguageBatchFinished']);

    $skipped = [];
    $validationError = [];
    $plugins = $this->uwMLSetupPLuginManger->getPluginsOrdered();
    if (isset($config['plugins'])) {
      $plugins = array_filter($plugins, function ($plugin) use ($config) {
        return in_array($plugin['id'], $config['plugins']);
      });
    }

    foreach ($plugins as $plugin_id => $plugin_definition) {

      /** @var \Drupal\uw_multilingual\UwMLSetupInterface $plugin_instance */
      $plugin_instance = $this->uwMLSetupPLuginManger->createInstance($plugin_id, $config);
      $plugin_operations_data = $plugin_instance->getData();
      $process = $plugin_instance->validateData();

      // Check if there is no data to be processed than skip the plugin.
      if (empty($plugin_operations_data)) {
        $skipped[] = [
          'plugin' => $plugin_id,
          'message' => 'Missing data to be processed or already processed',
        ];
        continue;
      }
      elseif ($process === UwMLSetupPluginBase::EXECUTION_STATUS['SKIP']) {
        $skipped[] = [
          'plugin' => $plugin_id,
          'message' => 'Plugin already executed.',
        ];
        continue;
      }

      // For multiple operations.
      // Create new operation for each item of data.
      if ($plugin_instance->multipleOperation()) {
        foreach ($plugin_operations_data as $plugin_data) {
          $batch_builder->addOperation(
            [$plugin_instance, 'processData'],
            [$plugin_data],
          );
        }
      }
      else {
        $batch_builder->addOperation(
          [$plugin_instance, 'processData'],
          [$plugin_operations_data],
        );
      }
    }

    if (!empty($validationError)) {
      return [
        'skipped' => $skipped,
        'validationErrors' => $validationError,
      ];
    }

    batch_set($batch_builder->toArray());
    $this->setDrupalBatches($config['language']);
    return [
      'batch' => $batch_builder->toArray(),
      'skipped' => $skipped,
      'validationError' => $validationError,
    ];
  }

  /**
   * Set Drupal batches of core/locale module.
   *
   * @param string $langcode
   *   The langcode to be set up.
   */
  protected function setDrupalBatches(string $langcode) {
    $localeSettings = $this->configFactory->get('locale.settings');
    $this->moduleHandler->loadInclude('locale', 'fetch.inc');
    $this->moduleHandler->loadInclude('locale', 'bulk.inc');
    $options = _locale_translation_default_update_options();
    if ($localeSettings->get('translation.import_enabled')) {
      // Download and import translations for the newly added language.
      batch_set(locale_translation_batch_update_build([], [$langcode], $options));
    }

    $files = $this->importCustomMigrationFiles($langcode);
    if (!empty($files)) {
      $preparedFiles = [];
      foreach ($files as $file) {
        $new_file = locale_translate_file_attach_properties($file, $options);
        $preparedFiles[$file->uri] = $new_file;
      }
      $batch = locale_translate_batch_build($preparedFiles, $options);
      batch_set($batch);
    }
    // Create or update all configuration translations for this language.
    if ($localeSettings->get('translation.import_enabled')) {
      $this->moduleHandler->loadInclude('locale', 'bulk.inc');
      if ($importBatch = locale_config_batch_update_components($options, [$langcode])) {
        batch_set($importBatch);
      }
    }
  }

  /**
   * Import custom translations.
   */
  protected function importCustomMigrationFiles($langcode) {
    $directory = $this->moduleHandler->getModule('uw_multilingual')
      ->getPath();

    // Create a Finder instance to find .po files in the directory.
    $finder = new Finder();
    $finder->files()->in("{$directory}/translations")
      ->name("*.{$langcode}.po");

    $files = [];
    // Iterate over the .po files and import each one.
    foreach ($finder as $file) {
      $stdFile = new \stdClass();
      $stdFile->filename = $file->getFilename();
      $stdFile->uri = $file->getPath() . '/' . $file->getFilename();
      $files[] = $stdFile;
    }
    return $files;
  }

  /**
   * Finish batch callback.
   */
  public static function updateContentLanguageBatchFinished($success, $results, $operations) {
    $messenger = \Drupal::messenger();
    if ($success) {
      $messenger->addMessage(t('Setup new language finished with success.'));
    }
    else {
      $messenger->addError(t('There was an error during node language update.'));
    }
  }

}
