<?php

namespace Drupal\uw_multilingual\Service;

/**
 * Interface to define methods of main service.
 */
interface UwMultiLingualSetupInterface {

  /**
   * Setup configuration necessary to change entire website to another language.
   *
   * @param array $config
   *   Configuration of the setup.
   */
  public function setupSiteConfiguration(array $config = []);

}
