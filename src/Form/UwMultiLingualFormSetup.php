<?php

namespace Drupal\uw_multilingual\Form;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\uw_multilingual\Service\UwMultiLingualSetupInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements the UwMultiLingualFormSetup form.
 */
class UwMultiLingualFormSetup extends FormBase {

  /**
   * Multilingual setup service.
   *
   * @var \Drupal\uw_multilingual\Service\UwMultiLingualSetupInterface
   */
  protected $uwMultiLingualSetup;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * File system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $uwMLSetupPLuginManger;

  /**
   * {@inheritdoc}
   */
  public function __construct(UwMultiLingualSetupInterface $uwMultiLingualSetupService, LanguageManagerInterface $language_manager, FileSystemInterface $fileSystem, PluginManagerInterface $uwMLSetupPluginManger) {
    $this->uwMultiLingualSetup = $uwMultiLingualSetupService;
    $this->languageManager = $language_manager;
    $this->fileSystem = $fileSystem;
    $this->uwMLSetupPLuginManger = $uwMLSetupPluginManger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('uw_multilingual.setup_new_language'),
      $container->get('language_manager'),
      $container->get('file_system'),
      $container->get('plugin.manager.uw_ml_setup')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'uw_multilingual_form_setup';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Add form elements here.
    $form['language_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Language settings'),
    ];

    $form['language_fieldset']['language'] = [
      '#type' => 'radios',
      '#title' => $this->t('Language'),
      '#options' => [
        'en' => $this->t('English'),
        'fr' => $this->t('French'),
        'zh-hans' => $this->t('Chinese'),
      ],
      '#default_value' => $this->languageManager->getDefaultLanguage()->getId(),
    ];

    $form['plugins_fieldset'] = [
      '#type' => 'details',
      '#title' => $this->t('Items to translate'),
      '#open' => FALSE,
    ];

    $form['plugins_fieldset']['execute_drupal_translation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Execute Drupal translation po files!'),
      '#default_value' => TRUE,
    ];

    $plugins = $this->uwMLSetupPLuginManger->getPluginsOrdered();

    $form['plugins_fieldset']['plugins'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Check plugins to execute'),
      '#options' => array_combine(
        array_column($plugins, 'id'),
        array_column($plugins, 'label')
      ),
      '#default_value' => array_column($plugins, 'id'),
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    try {
      $plugins = array_filter($form_state->getValue('plugins'));
      $result = $this->uwMultiLingualSetup->setupSiteConfiguration([
        'language' => $form_state->getValue('language'),
        'plugins' => $plugins,
        'execute_drupal_translation' => $form_state->getValue('execute_drupal_translation'),
      ]);
    }
    catch (\Exception $exception) {
      $this->messenger()
        ->addError($exception->getMessage());
    }

    if (!empty($result['skipped'])) {
      foreach ($result['skipped'] as $plugin) {
        $this->messenger()
          ->addWarning($this->t('The plugin @plugin_id was not executed: @message', [
            '@plugin' => $plugin['plugin'],
            '@message' => $plugin['message'],
          ]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $translationsDirectory = 'translations://';
    if (!$this->fileSystem->prepareDirectory($translationsDirectory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
      $form_state->setErrorByName('language', $this->t('The directory translations:// is not writable or not configured!'));
    }
  }

}
