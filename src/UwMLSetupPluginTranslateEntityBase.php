<?php

namespace Drupal\uw_multilingual;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base plugin for plugins that work with entity data.
 */
abstract class UwMLSetupPluginTranslateEntityBase extends UwMLSetupPluginBase {

  /**
   * Entity storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ContainerInterface $container) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $container);
    $this->storage = $container->get('entity_type.manager')->getStorage($this->getEntityType());
  }

  /**
   * Define entity type to be processed.
   *
   * @return string
   *   The entity type id.
   */
  abstract public function getEntityType(): string;

  /**
   * Query database looking for entities with the type defined.
   *
   * @return array
   *   Array of entities id.
   */
  public function getData(): array {
    $query = $this->storage->getQuery();
    $criteria = $this->getCriteria();
    if (!empty($criteria)) {
      foreach ($criteria as $field => $criteriaData) {
        $query->condition($field, $criteriaData[0], $criteriaData[1]);
      }
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function processData($entity_id, &$context = []) {
    $entity = $this->storage->load($entity_id);
    $entity->set($this->getLanguageColumnName(), $this->configuration['language']);
    $entity->save();
    $context['message'] = $this->t('Updating language entity type @type of content @label.',
      [
        '@label' => $entity->label(),
        '@type' => $this->getEntityType(),
      ]
    );
  }

  /**
   * Return the criteria to be used on thq query in processData method.
   *
   * Query only the entities that as different langcode.
   *
   * @return array[]
   *   Array of criteria.
   */
  public function getCriteria() {
    return [
      $this->getLanguageColumnName() => [$this->configuration['language'], '<>'],
    ];
  }

  /**
   * Define the column name to get the language code.
   *
   * @return string
   *   Column name.
   */
  public function getLanguageColumnName() {
    return 'langcode';
  }

}
