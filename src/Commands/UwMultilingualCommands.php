<?php

namespace Drupal\uw_multilingual\Commands;

use Drupal\uw_multilingual\Service\UwMultiLingualSetupInterface;
use Drush\Commands\DrushCommands;

/**
 * Drush commands for uw_multilingual module.
 *
 * @package Drupal\uw_multilingual\Commands
 */
class UwMultilingualCommands extends DrushCommands {

  /**
   * UW Multilingual service.
   *
   * @var \Drupal\uw_multilingual\Service\UwMultiLingualSetupInterface
   */
  protected $uwMultiLingualService;

  /**
   * {@inheritDoc}
   */
  public function __construct(UwMultiLingualSetupInterface $uwMultiLingualService) {
    $this->uwMultiLingualService = $uwMultiLingualService;
  }

  /**
   * Call service of multilingual module to set up language settings.
   *
   * @param string $language_code
   *   The language code to update the entities to.
   *
   * @command uw:update-language
   * @aliases uwul
   * @usage uw:update-language fr
   *   - Create the FR language and configure all settings.
   *
   * @throws \Exception
   */
  public function updateLanguage(string $language_code) {
    try {
      $this->uwMultiLingualService->setupSiteConfiguration([
        'language' => $language_code,
      ]);
      drush_backend_batch_process();
    }
    catch (\Exception $exception) {
      $this->logger()->error($exception->getMessage());
    }
  }

}
