<?php

namespace Drupal\uw_multilingual;

/**
 * Interface to define plugin type uw_ml_setup_info behavior.
 */
interface UwMLSetupInterface {

  /**
   * Plugin id.
   *
   * @return string
   *   The plugin id.
   */
  public function id();

  /**
   * Plugin label.
   *
   * @return string
   *   The plugin label.
   */
  public function label();

  /**
   * Description of the plugin.
   *
   * @return string
   *   The plugin description.
   */
  public function description();

  /**
   * Weight value that the plugin should be executed.
   *
   * @return int
   *   The weight of the plugin.
   */
  public function weight();

  /**
   * Label to displayed on the batch whe the plugin is executing.
   *
   * @return string
   *   The label to display on batch plugin execution.
   */
  public function batchLabel();

  /**
   * Flag to control how the plugin will create the operations.
   *
   * True:
   *  For each data returned on getData function will be an operation.
   * False:
   *  Only one operation will be created to the getData returns.
   *
   * @return bool
   *   The true to execute in multiple operations. False to crate one operation.
   */
  public function multipleOperation();

  /**
   * Return the data / information to be processed / configured.
   *
   * @return array
   *   Array of data to be processed by process data.
   */
  public function getData(): array;

  /**
   * Process/Configure the data of the plugin.
   *
   * @param mixed $data
   *   Value returned from the getData method.
   * @param mixed &$context
   *   Batch context.
   */
  public function processData($data, &$context = []);

  /**
   * Validate if the plugin is already good to be executed.
   *
   * @return array
   *   Array of errors.
   */
  public function validateData();

}
