<?php

namespace Drupal\uw_multilingual;

use Drupal\Component\Plugin\Discovery\DerivativeDiscoveryDecorator;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Plugin\Discovery\AnnotatedClassDiscovery;

/**
 * Plugin manager for UW Multilingual type.
 */
class UwMLSetupPluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cacheBackend, ModuleHandlerInterface $module_handler) {
    $subdir = 'Plugin/UwMLSetup';
    $plugin_interface = 'Drupal\uw_multilingual\UwMLSetupInterface';
    $plugin_definition_annotation_name = 'Drupal\uw_multilingual\Annotation\UwMLSetup';
    parent::__construct($subdir, $namespaces, $module_handler, $plugin_interface, $plugin_definition_annotation_name);
    $this->alterInfo('uw_ml_setup_info');
    $this->setCacheBackend($cacheBackend, 'uw_ml_setup_info');
    $discovery = new AnnotatedClassDiscovery($this->subdir, $this->namespaces, $this->pluginDefinitionAnnotationName, $this->additionalAnnotationNamespaces);
    $this->discovery = new DerivativeDiscoveryDecorator($discovery);
  }

  /**
   * Return the plugins by weight of executions.
   *
   * Each plugin has a weight config value.
   * The method will weight the executions plugins by this value.
   *
   * @return array
   *   Array of the plugins in weight.
   */
  public function getPluginsOrdered(): array {
    $definitions = $this->getDefinitions();
    uasort($definitions, function ($a, $b) {
      return $a['weight'] - $b['weight'];
    });
    return $definitions;
  }

}
